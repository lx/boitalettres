use miette::{IntoDiagnostic, Result};

use boitalettres::proto::{Request, Response};
use boitalettres::server::accept::addr::{AddrIncoming, AddrStream};
use boitalettres::server::Server;

async fn handle_req(_req: Request) -> Result<Response> {
    use imap_codec::types::response::{Capability, Data as ImapData};

    use boitalettres::proto::res::{body::Data, Status};

    let capabilities = vec![Capability::Imap4Rev1, Capability::Idle];
    let body: Vec<Data> = vec![
        Status::ok("Yeah")?.into(),
        ImapData::Capability(capabilities).into(),
    ];

    Ok(Response::ok("Done")?.with_body(body))
}

#[tokio::main]
async fn main() -> Result<()> {
    setup_logging();

    let incoming = AddrIncoming::new("127.0.0.1:4567")
        .await
        .into_diagnostic()?;

    let make_service = tower::service_fn(|addr: &AddrStream| {
        tracing::debug!(remote_addr = %addr.remote_addr, local_addr = %addr.local_addr, "accept");

        let service = tower::ServiceBuilder::new().service_fn(handle_req);

        futures::future::ok::<_, std::convert::Infallible>(service)
    });

    let server = Server::new(incoming).serve(make_service);

    server.await.map_err(Into::into)
}

// Don't mind this, this is just for debugging.
fn setup_logging() {
    use tracing_subscriber::prelude::*;

    let registry = tracing_subscriber::registry().with(
        tracing_subscriber::fmt::layer().with_filter(
            tracing_subscriber::filter::Targets::new()
                .with_default(tracing::Level::DEBUG)
                .with_target("boitalettres", tracing::Level::TRACE)
                .with_target("simple", tracing::Level::TRACE)
                .with_target("tower", tracing::Level::TRACE)
                .with_target("tokio_tower", tracing::Level::TRACE)
                .with_target("mio", tracing::Level::TRACE),
        ),
    );

    #[cfg(tokio_unstable)]
    let registry = registry.with(console_subscriber::spawn());

    registry.init();
}
