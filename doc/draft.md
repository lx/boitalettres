# Just Enough Doc

## Run `simple` example

This example is meant to show basic service-based IMAP server with boitalettres library.

- [Source code](../examples/simple.rs)

```shell
$ cargo run --example simple
```

If you want to trace this `simple` example with the [`tokio-console`](https://github.com/tokio-rs/console/tree/main/tokio-console) utility, run:

```shell
$ RUSTFLAGS="tokio_unstable" cargo run --example simple --features "tokio/tracing"
```

- [Basic python testing script](../scripts/test_imap.py)

```shell
$ python scripts/test_imap.py
```

## Notes

- The library use the `tracing` library for logging, a basic setup is shown in the `simple` example with the `tracing-subscriber` crate.

## References

- [`imap-codec`](https://docs.rs/imap-codec): IMAP protocol model+encoder/decoder
- [`tower`](https://docs.rs/tower): Service-based request handling library
