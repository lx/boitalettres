use futures::prelude::*;
use imap_codec::types::core::Tag;

use super::body::Data;
use super::Response;

pub fn response_stream(res: Response, tag: Option<Tag>) -> impl Stream<Item = Data> {
    let close = res.close;
    let (body, status) = res.split();
    let body = body.map(|body| body.into_stream());

    async_stream::stream! {
        if let Some(body) = body {
            for await item in body {
                tracing::trace!(?item, "response_stream.yield");
                yield item;
            }
        }

        let item = status.into_imap(tag);
        tracing::trace!(?item, "response_stream.yield");
        yield item.into();

        if close {
            yield Data::Close;
        }
    }
}
