use imap_codec::types::command::Command;

#[derive(Debug)]
pub struct Request {
    pub command: Command,
}
