use miette::Diagnostic;

type BoxedError = tower::BoxError;

#[derive(Debug, thiserror::Error, Diagnostic)]
pub enum Error {
    #[error("Error occured when accepting new connections")]
    #[diagnostic(code(boitalettres::accept))]
    Accept(#[source] BoxedError),

    #[error("Error occured on service creation")]
    #[diagnostic(code(boitalettres::make_service))]
    MakeService(#[source] BoxedError),

    #[error("{0}")]
    Text(String),
}

impl Error {
    pub(crate) fn accept<E: Into<BoxedError>>(err: E) -> Error {
        Error::Accept(err.into())
    }

    pub(crate) fn make_service<E: Into<BoxedError>>(err: E) -> Error {
        Error::MakeService(err.into())
    }

    pub(crate) fn text<E: Into<String>>(err: E) -> Error {
        Error::Text(err.into())
    }
}

pub type Result<T, E = Error> = std::result::Result<T, E>;
