use std::future::Future;
use std::task::Context;
use std::task::Poll;

use tower::Service;

pub trait MakeServiceRef<Target, Request>: self::sealed::Sealed<(Target, Request)> {
    type Response;
    type Error;
    type Service: Service<Request, Response = Self::Response, Error = Self::Error>;

    type MakeError;
    type Future: Future<Output = Result<Self::Service, Self::MakeError>>;

    fn poll_ready_ref(&mut self, cx: &mut Context) -> Poll<Result<(), Self::MakeError>>;
    fn make_service_ref(&mut self, target: &Target) -> Self::Future;
}

impl<M, S, Target, Request> self::sealed::Sealed<(Target, Request)> for M
where
    M: for<'a> Service<&'a Target, Response = S>,
    S: Service<Request>,
{
}

impl<M, ME, MF, S, Target, Request> MakeServiceRef<Target, Request> for M
where
    M: for<'a> Service<&'a Target, Response = S, Future = MF, Error = ME>,
    MF: Future<Output = Result<S, ME>>,
    S: Service<Request>,
{
    type Response = S::Response;
    type Error = S::Error;
    type Service = S;

    type MakeError = ME;
    type Future = MF;

    fn poll_ready_ref(&mut self, cx: &mut Context) -> Poll<Result<(), Self::MakeError>> {
        self.poll_ready(cx)
    }

    fn make_service_ref(&mut self, target: &Target) -> Self::Future {
        self.call(target)
    }
}

mod sealed {
    pub trait Sealed<T> {}
}
